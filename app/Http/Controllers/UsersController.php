<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class UsersController extends Controller
{
    public function getlist()
    {
        $data = User::paginate(10);
        return view('back-end.users.list', ['data'=>$data]);
    }
    public function getedit($id)
    {
        $data = User::where('id', $id)->first();
        return view('back-end.users.edit', ['data'=>$data]);
    }
   
    public function postedit($id, Request $req)
    {
      //   dd($req->all());
        // dd($id);
        $user = User::findOrFail($id);
        if ($user) {
            $user->name = $req->txtName;
            $user->save();
            return redirect('admin/khachhang')
         ->with(['flash_level'=>'result_msg','flash_massage'=>' Đã lưu !']);
        }
    }
    
    public function getdel($id)
    {
        User::destroy($id);
        return redirect('admin/khachhang')
        ->with(['flash_level'=>'result_msg','flash_massage'=>' Đã xóa !']);
    }

}
